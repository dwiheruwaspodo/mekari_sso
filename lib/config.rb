# frozen_string_literal: true

module MekariSso
  class Config
    attr_accessor :client_id, :client_secret, :base_url, :access_token, :accept_language, :scope

    # AVAILABLE_KEYS = %i[client_id client_secret base_url access_token accept_language].freeze

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      %i[client_id client_secret base_url access_token accept_language scope]
    end

    def apply(hash)
      hash.each do |key, value|
        unless available_keys.include?(key.to_s.to_sym)
          raise ArgumentError,
                "Unknown option #{key.inspect},
                available keys: #{available_keys.map(&:inspect).join(', ')}"
        end

        send(:"#{key}=", value)
      end
    end

    def validate_required!
      unless @client_id.present?
        raise ArgumentError, "Params client_id is required options"
      end

      unless @client_secret.present?
        raise ArgumentError, "Params client_secret is required options"
      end

      unless @base_url.present?
        raise ArgumentError, "Params base_url is required options"
      end 
    end
  end
end
