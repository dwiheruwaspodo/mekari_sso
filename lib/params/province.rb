# frozen_string_literal: true

require 'config'
require_relative 'list'

module MekariSso
  class Province < MekariSso::List
    attr_accessor :province_id

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      super + %i[province_id]
    end

    def validate_required!
      unless @province_id.present?
        raise ArgumentError, "Params province_id is required options"
      end
    end
  end
end
