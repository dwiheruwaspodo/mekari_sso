# frozen_string_literal: true

require 'config'

module MekariSso
  class RemoveUserCompany < MekariSso::AddUserCompany
  end
end
