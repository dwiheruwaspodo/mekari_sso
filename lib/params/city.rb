# frozen_string_literal: true

require 'config'

module MekariSso
  class City < MekariSso::Config
    attr_accessor :city_id

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      %i[city_id access_token]
    end

    def validate_required!
      unless @city_id.present?
        raise ArgumentError, "Params city_id is required options"
      end

      unless @access_token.present?
        raise ArgumentError, "Params access_token is required options"
      end
    end
  end
end
