# frozen_string_literal: true

require 'config'
require_relative 'user_register'

module MekariSso
  class UserUpdate < MekariSso::UserRegister
    attr_accessor :email, :first_name, :last_name, :password, :phone, :access_token, :user_id

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      super + %i[user_id]
    end

    def validate_required!
      unless @user_id.present?
        raise ArgumentError, "Params user_id is required options"
      end 

      unless @access_token.present?
        raise ArgumentError, "Params access_token is required options"
      end 
    end
  end
end
