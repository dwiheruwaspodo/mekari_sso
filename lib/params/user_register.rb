# frozen_string_literal: true

require 'config'

module MekariSso
  class UserRegister < MekariSso::Config
    attr_accessor :email, :first_name, :last_name, :password, :phone, :access_token

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      %i[email first_name last_name password phone access_token]
    end

    def validate_required!
      unless @email.present?
        raise ArgumentError, "Params email is required options"
      end

      unless @first_name.present?
        raise ArgumentError, "Params first_name is required options"
      end

      unless @password.present?
        raise ArgumentError, "Params password is required options"
      end 

      unless @phone.present?
        raise ArgumentError, "Params phone is required options"
      end 

      unless @access_token.present?
        raise ArgumentError, "Params access_token is required options"
      end 
    end
  end
end
