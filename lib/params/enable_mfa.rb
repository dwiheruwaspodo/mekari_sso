# frozen_string_literal: true

require 'config'

module MekariSso
  class EnableMfa < MekariSso::Config
    attr_accessor :company_id, :user_id, :channel, :access_token

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      %i[company_id user_id channel access_token]
    end

    def validate_required!
      unless @company_id.present?
        raise ArgumentError, "Params company_id is required options"
      end

      unless @user_id.present?
        raise ArgumentError, "Params user_id is required options"
      end

      unless @access_token.present?
        raise ArgumentError, "Params access_token is required options"
      end

      unless @channel.present? && ["email", "sms", "whatsapp"].include?(@channel)
        raise ArgumentError, "Params channel is required options"
      end
    end
  end
end
