# frozen_string_literal: true

require 'config'

module MekariSso
  class CurrentCompanyOfUser < MekariSso::CompanyOfUser
  end
end
