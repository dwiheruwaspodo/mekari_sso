# frozen_string_literal: true

require 'config'

module MekariSso
  class CompanyRegister < MekariSso::Config
    attr_accessor :name, :email, :industry_id, :industry_name, :user_id, :brand_name, :main_address, :access_token,
                  :secondary_address, :phone, :tax_name, :tax_number, :billing_name, :billing_email, :billing_phone

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      %i[
        access_token
        name
        email
        industry_id
        industry_name
        user_id
        brand_name
        main_address
        secondary_address
        phone
        tax_name
        tax_number
        billing_name
        billing_email
        billing_phone
      ]
    end

    def validate_required!
      unless @name.present?
        raise ArgumentError, "Params name is required options"
      end

      unless @email.present?
        raise ArgumentError, "Params email is required options"
      end

      unless @user_id.present?
        raise ArgumentError, "Params user_id is required options"
      end
    end
  end
end
