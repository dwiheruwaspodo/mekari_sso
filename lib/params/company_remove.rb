# frozen_string_literal: true

require 'config'

module MekariSso
  class CompanyRemove < MekariSso::Config
    attr_accessor :access_token, :company_id, :user_id

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      %i[company_id user_id access_token]
    end

    def validate_required!
      unless @access_token.present?
        raise ArgumentError, "Params access_token is required options"
      end

      unless @company_id.present?
        raise ArgumentError, "Params company_id is required options"
      end

      unless @user_id.present?
        raise ArgumentError, "Params user_id is required options"
      end 
    end
  end
end
