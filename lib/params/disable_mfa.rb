# frozen_string_literal: true

require 'config'

module MekariSso
  class DisableMfa < MekariSso::EnableMfa
  end
end
