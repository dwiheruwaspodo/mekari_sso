# frozen_string_literal: true

require 'config'

module MekariSso
  class List < MekariSso::Config
    attr_accessor :access_token, :page, :limit

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      %i[access_token page limit]
    end

    def validate_required!
      unless @access_token.present?
        raise ArgumentError, "Params access_token is required options"
      end 
    end
  end
end
