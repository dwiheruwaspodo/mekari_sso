# frozen_string_literal: true

require 'config'

module MekariSso
  class Company < MekariSso::Config
    attr_accessor :access_token, :page, :company_id

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      %i[access_token company_id]
    end

    def validate_required!
      unless @access_token.present?
        raise ArgumentError, "Params access_token is required options"
      end 

      unless @company_id.present?
        raise ArgumentError, "Params company_id is required options"
      end 
    end
  end
end
