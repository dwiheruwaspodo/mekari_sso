# frozen_string_literal: true

require 'config'

module MekariSso
  class CompanyUpdate < MekariSso::CompanyRegister
    attr_accessor :name, :email, :industry_id, :industry_name, :user_id, :brand_name, :main_address, :access_token,
                  :secondary_address, :phone, :tax_name, :tax_number, :billing_name, :billing_email, :billing_phone,
                  :company_id

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      super + %i[company_id]
    end

    def validate_required!
      unless @name.present?
        raise ArgumentError, "Params name is required options"
      end

      unless @email.present?
        raise ArgumentError, "Params email is required options"
      end

      unless @user_id.present?
        raise ArgumentError, "Params user_id is required options"
      end

      unless @company_id.present?
        raise ArgumentError, "Params company_id is required options"
      end
    end
  end
end
