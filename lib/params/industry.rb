# frozen_string_literal: true

require 'config'
require_relative 'list'

module MekariSso
  class Industry < MekariSso::List
    attr_accessor :version

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      super + %i[version]
    end

  end
end
