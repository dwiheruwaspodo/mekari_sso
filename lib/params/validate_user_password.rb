# frozen_string_literal: true

require 'config'
require_relative 'user_register'

module MekariSso
  class ValidateUserPassword < MekariSso::Config
    attr_accessor :password, :access_token, :user_id

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      %i[user_id access_token password]
    end

    def validate_required!
      unless @access_token.present?
        raise ArgumentError, "Params access_token is required options"
      end 

      unless @user_id.present?
        raise ArgumentError, "Params user_id is required options"
      end 

      unless @password.present?
        raise ArgumentError, "Params password is required options"
      end 
    end
  end
end
