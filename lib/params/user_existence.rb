# frozen_string_literal: true

require 'config'

module MekariSso
  class UserExistence < MekariSso::Config
    attr_accessor :key, :value, :access_token

    def initialize(options = nil)
      apply(options) if options
    end

    def available_keys
      %i[key value access_token]
    end

    def validate_required!
      unless @key.present?
        raise ArgumentError, "Params key is required options"
      end

      unless @value.present?
        raise ArgumentError, "Params value is required options"
      end

      unless @access_token.present?
        raise ArgumentError, "Params access_token is required options"
      end 
    end
  end
end
