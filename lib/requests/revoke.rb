# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class Revoke < MekariSso::HttpRequest

    attr_reader :config, :token

    def initialize(config, token)
      @config = config
      @token = token
    end

    def headers_request
      headers.merge({
        authorization: "Bearer #{@token}"
      })
    end

    def send
      RestClient.delete "#{@config.base_url}/auth/oauth2/token", headers_request
    end
  end
  
end
