# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class GenerateCredentialClient < MekariSso::HttpRequest

    attr_reader :config 

    def initialize(config)
      @config = config
    end

    def params
      {
        client_id: @config.client_id,
        client_secret: @config.client_secret,
        grant_type: 'client_credentials',
        scope: @config.scope || 'sso:profile'
      }
    end

    def send
      RestClient.post "#{@config.base_url}/auth/oauth2/token", params
    end
  end
  
end
