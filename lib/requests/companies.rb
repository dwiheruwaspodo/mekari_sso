# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class Companies < MekariSso::HttpRequest

    attr_reader :config, :params

    def initialize(config, params)
      @config = config
      @params = params
    end

    def headers_request
      headers.merge({
        authorization: "Bearer #{@params.access_token}",
        accept_language: @config.accept_language
      })
    end

    def page
      @params.page.present? ? @params.page : 1
    end

    def send
      RestClient.get "#{@config.base_url}/v1.1/companies?page=#{page}", headers_request
    end
  end
  
end
