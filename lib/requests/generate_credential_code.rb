# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class GenerateCredentialCode < MekariSso::HttpRequest

    attr_reader :config, :code

    def initialize(config, code)
      @config = config
      @code = code
    end

    def params
      {
        client_id: @config.client_id,
        client_secret: @config.client_secret,
        grant_type: 'authorization_code',
        code: @code
      }
    end

    def send
      RestClient.post "#{@config.base_url}/auth/oauth2/token", params
    end
  end
  
end
