# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class Refresh < MekariSso::HttpRequest

    attr_reader :config, :code

    def initialize(config, refresh_token)
      @config = config
      @refresh_token = refresh_token
    end

    def params
      {
        client_id: @config.client_id,
        client_secret: @config.client_secret,
        grant_type: 'refresh_token',
        refresh_token: @refresh_token
      }
    end

    def send
      RestClient.post "#{@config.base_url}/auth/oauth2/token", params
    end
  end
  
end
