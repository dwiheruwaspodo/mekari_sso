# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class UpdateAvatar < MekariSso::HttpRequest

    attr_reader :config, :params

    def initialize(config, params)
      @config = config
      @params = params
    end

    def params
      {
        avatar: @params.avatar
      }
    end

    def headers_request
      {
        authorization: "Bearer #{@params.access_token}",
        accept_language: @config.accept_language
      }
    end

    def send
      RestClient.post "#{@config.base_url}/v1/users/#{@params.user_id}/avatar", params, headers_request
    end
  end
  
end
