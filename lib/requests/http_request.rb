# frozen_string_literal: true

require 'json'

module MekariSso
  class HttpRequest
    def send; end

    def params; end
    
    def headers
      {
        accept: :json,
        content_type: :json
      }
    end

    def after_success(data)
      result = Struct.new(:response)
      result.new(data)
    end

    def after_error(data, error)
      result = Struct.new(:response, :error)
      result.new(data, error)
    end

    def original_response
      @with_original_response = true
      self
    end

    def handle_response(response)
      if @with_original_response
        begin
          return JSON.parse(response.body)
        rescue StandardError
          return response.body
        end
      end

      result = JSON.parse(response.body)

      after_success(result)
    rescue
      response
    end

    def handle_error(response, error)
      result = JSON.parse(response)

      after_error(result, error.to_s)
    rescue
      after_error(result, error.to_s)
    end
  end
end
