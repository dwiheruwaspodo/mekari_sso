# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class AddUserToCompany < MekariSso::HttpRequest

    attr_reader :config, :params

    def initialize(config, params)
      @config = config
      @params = params
    end

    def params
      {
        user_id: @params.user_id
      }
    end

    def headers_request
      headers.merge({
        authorization: "Bearer #{@params.access_token}",
        accept_language: @config.accept_language
      })
    end

    def send
      RestClient.post "#{@config.base_url}/v1/companies/#{@params.company_id}/users", params, headers_request
    end
  end
  
end
