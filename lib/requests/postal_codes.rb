# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class PostalCodes < MekariSso::HttpRequest

    attr_reader :config, :params

    def initialize(config, params)
      @config = config
      @params = params
    end

    def headers_request
      headers.merge({
        authorization: "Bearer #{@params.access_token}",
        accept_language: @config.accept_language
      })
    end

    def send
      RestClient.get "#{@config.base_url}/v1.1/cities/#{@params.city_id}/postal_codes", headers_request
    end
  end
  
end
