# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class MfaEnable < MekariSso::HttpRequest

    attr_reader :config, :params

    def initialize(config, params)
      @config = config
      @params = params
    end

    def params
      {
        channel: @params.channel
      }
    end

    def headers_request
      headers.merge({
        authorization: "Bearer #{@params.access_token}",
        accept_language: @config.accept_language
      })
    end

    def send
      RestClient.post "#{@config.base_url}/v1.1/companies/#{@params.company_id}/users/#{@params.user_id}/2fa/enable", params, headers_request
    end
  end
  
end
