# frozen_string_literal: true

require 'rest-client'
require 'cgi'
require 'uri'
require_relative 'http_request'

module MekariSso
  class CheckUserExistence < MekariSso::HttpRequest

    attr_reader :config, :params

    def initialize(config, params)
      @config = config
      @params = params
    end

    def params
      {
        attr_key: @params.key,
        attr_value: @params.value,
      }
    end

    def headers_request
      headers.merge({
        authorization: "Bearer #{@params.access_token}",
        accept_language: @config.accept_language
      })
    end

    def send
      # url = URI("#{@config.base_url}/v1/users/check?attr_key=#{@params.key}&attr_value=#{CGI.escape(@params.value)}")
      url = URI("#{@config.base_url}/v1/users/check")
      url.query = URI.encode_www_form(params)

      RestClient.get url.to_s, headers_request
    end
  end
  
end
