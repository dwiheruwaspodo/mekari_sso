# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class Industries < MekariSso::HttpRequest

    attr_reader :config, :params

    def initialize(config, params)
      @config = config
      @params = params
    end

    def headers_request
      headers.merge({
        authorization: "Bearer #{@params.access_token}",
        accept_language: @config.accept_language
      })
    end

    def send
      RestClient.get "#{@config.base_url}/#{version}/industries?page=#{page}&limit=#{limit}", headers_request
    end

    def version 
      params.version.present? ? @params.version : 'v1'
    end

    def page
      @params.page.present? ? @params.page : 1
    end

    def limit 
      @params.limit.present? ? @params.limit : 10
    end
  end
  
end
