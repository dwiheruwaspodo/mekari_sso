# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class CurrentCompany < MekariSso::HttpRequest

    attr_reader :config, :token

    def initialize(config, token)
      @config = config
      @token = token
    end

    def headers_request
      headers.merge({
        authorization: "Bearer #{@token}",
        accept_language: @config.accept_language
      })
    end

    def send
      RestClient.get "#{@config.base_url}/v1.1/users/me/current_company", headers_request
    end
  end
  
end
