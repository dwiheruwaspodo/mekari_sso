# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class UpdateCompany < MekariSso::HttpRequest

    attr_reader :config, :params

    def initialize(config, params)
      @config = config
      @params = params
    end

    def params
      {
        name: @params.name,
        email: @params.email,
        industry_id: @params.industry_id,
        industry_name: @params.industry_name,
        user_id: @params.user_id,
        brand_name: @params.brand_name,
        main_address: @params.main_address,
        secondary_address: @params.secondary_address,
        phone: @params.phone,
        tax_name: @params.tax_name,
        tax_number: @params.tax_number,
        billing_name: @params.billing_name,
        billing_email: @params.billing_email,
        billing_phone: @params.billing_phone
      }
    end

    def headers_request
      headers.merge({
        authorization: "Bearer #{@params.access_token}",
        accept_language: @config.accept_language
      })
    end

    def send
      RestClient.put "#{@config.base_url}/v1/companies/#{@params.company_id}", params, headers_request
    end
  end
  
end
