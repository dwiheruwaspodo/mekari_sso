# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class RegisterUser < MekariSso::HttpRequest

    attr_reader :config, :params

    def initialize(config, params)
      @config = config
      @params = params
    end

    def params
      {
        email: @params.email,
        first_name: @params.first_name,
        last_name: @params.last_name,
        password: @params.password,
        phone: @params.phone
      }
    end

    def headers_request
      headers.merge({
        authorization: "Bearer #{@params.access_token}",
        accept_language: @config.accept_language
      })
    end

    def send
      RestClient.post "#{@config.base_url}/v1/users/", params, headers_request
    end
  end
  
end
