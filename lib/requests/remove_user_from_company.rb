# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class RemoveUserFromCompany < MekariSso::HttpRequest

    attr_reader :config, :params

    def initialize(config, params)
      @config = config
      @params = params
    end

    def headers_request
      headers.merge({
        authorization: "Bearer #{@params.access_token}",
        accept_language: @config.accept_language
      })
    end

    def send
      RestClient.delete "#{@config.base_url}/v1/companies/#{@params.company_id}/users/#{@params.user_id}", headers_request
    end
  end
  
end
