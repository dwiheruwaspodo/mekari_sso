# frozen_string_literal: true

require 'rest-client'
require_relative 'http_request'

module MekariSso
  class CurrentUser < MekariSso::HttpRequest

    attr_reader :config, :token

    def initialize(config, token)
      @config = config
      @token = token
    end

    def headers_request
      headers.merge({
        authorization: "Bearer #{@token}",
        accept_language: @config.accept_language
      })
    end

    def send
      RestClient.get "#{@config.base_url}/v1/users/me", headers_request
    end
  end
  
end
