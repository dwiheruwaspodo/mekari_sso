# frozen_string_literal: true

require_relative 'config'
require 'requests/add_user_to_company'
require 'requests/check_user_existence'
require 'requests/cities'
require 'requests/companies_of_user'
require 'requests/companies'
require 'requests/company_current_of_user'
require 'requests/company_member'
require 'requests/current_user'
require 'requests/current_company'
require 'requests/generate_credential_client'
require 'requests/generate_credential_code'
require 'requests/industries'
require 'requests/postal_codes'
require 'requests/provincies'
require 'requests/refresh'
require 'requests/revoke'
require 'requests/sender'
require 'requests/register_company'
require 'requests/register_user'
require 'requests/remove_company'
require 'requests/remove_user_from_company'
require 'requests/update_user'
require 'requests/update_avatar'
require 'requests/update_company'
require 'requests/verify_email_user'
require 'requests/validate_password'
require 'requests/mfa_enable'
require 'requests/mfa_disable'

require 'params/add_user_company'
require 'params/city'
require 'params/user_register'
require 'params/company_of_user'
require 'params/company_register'
require 'params/company_remove'
require 'params/company_update'
require 'params/company'
require 'params/current_company_of_user'
require 'params/industry'
require 'params/province'
require 'params/remove_user_company'
require 'params/list'
require 'params/user_existence'
require 'params/user_update'
require 'params/user_avatar'
require 'params/user_verify_email'
require 'params/validate_user_password'
require 'params/enable_mfa'
require 'params/disable_mfa'

require 'active_support'
require 'active_support/core_ext'

module MekariSso
  class Client
    attr_accessor :options

    def initialize(options = nil)
      @options = MekariSso::Config.new(options)
      @options.validate_required!
    end

    def generate_client
      sender = MekariSso::Sender.new
      sender >> MekariSso::GenerateCredentialClient.new(@options)
    end

    def generate_code(code)
      sender = MekariSso::Sender.new
      sender >> MekariSso::GenerateCredentialCode.new(@options, code)
    end

    def refresh_token(token)
      sender = MekariSso::Sender.new
      sender >> MekariSso::Refresh.new(@options, token)
    end

    def revoke_token(token)
      sender = MekariSso::Sender.new
      sender >> MekariSso::Revoke.new(@options, token)
    end
    
    def current_user(token)
      sender = MekariSso::Sender.new
      sender >> MekariSso::CurrentUser.new(@options, token)
    end

    def current_company(token)
      sender = MekariSso::Sender.new
      sender >> MekariSso::CurrentCompany.new(@options, token)
    end

    def industries(params)
      industri = MekariSso::Industry.new(params)
      industri.validate_required!
      
      sender = MekariSso::Sender.new
      sender >> MekariSso::Industries.new(@options, industri)
    end

    def register(params = {})
      user = MekariSso::UserRegister.new(params)
      user.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::RegisterUser.new(@options, user)
    end

    def register_company(params = {})
      company = MekariSso::CompanyRegister.new(params)
      company.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::RegisterCompany.new(@options, company)
    end

    def update_company(params = {})
      company = MekariSso::CompanyUpdate.new(params)
      company.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::UpdateCompany.new(@options, company)
    end

    def remove_company(params)
      company = MekariSso::CompanyRemove.new(params)
      company.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::RemoveCompany.new(@options, company)
    end

    def companies(params)
      company = MekariSso::List.new(params)
      company.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::Companies.new(@options, company)
    end

    def provincies(params)
      prov = MekariSso::List.new(params)
      prov.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::Provincies.new(@options, prov)
    end

    def cities(params)
      cities = MekariSso::Province.new(params)
      cities.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::Cities.new(@options, cities)
    end

    def postal_codes(params)
      postal_code = MekariSso::City.new(params)
      postal_code.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::PostalCodes.new(@options, postal_code)
    end

    def add_user_to_company(params)
      user = MekariSso::AddUserCompany.new(params)
      user.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::AddUserToCompany.new(@options, user)
    end

    def remove_user_from_company(params)
      user = MekariSso::RemoveUserCompany.new(params)
      user.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::RemoveUserFromCompany.new(@options, user)
    end

    def company_member(params)
      user = MekariSso::Company.new(params)
      user.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::CompanyMember.new(@options, user)
    end

    def update_user(params)
      user = MekariSso::UserUpdate.new(params)
      user.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::UpdateUser.new(@options, user)
    end

    def update_avatar(params)
      user = MekariSso::UserAvatar.new(params)
      user.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::UpdateAvatar.new(@options, user)
    end

    def verify_user_email(params)
      user = MekariSso::UserVerifyEmail.new(params)
      user.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::VerifyEmailUser.new(@options, user)
    end

    def existence_user(params)
      user = MekariSso::UserExistence.new(params)
      user.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::CheckUserExistence.new(@options, user)
    end

    def validate_password(params)
      password = MekariSso::ValidateUserPassword.new(params)
      password.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::ValidatePassword.new(@options, password)
    end

    def companies_of_user(params)
      company = MekariSso::CompanyOfUser.new(params)
      company.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::CompaniesOfUser.new(@options, company)
    end
    
    def current_company_of_user(params)
      company = MekariSso::CurrentCompanyOfUser.new(params)
      company.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::CompanyCurrentOfUser.new(@options, company)
    end

    def mfa_enable(params)
      user = MekariSso::EnableMfa.new(params)
      user.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::MfaEnable.new(@options, user)
    end

    def mfa_disable(params)
      user = MekariSso::DisableMfa.new(params)
      user.validate_required!

      sender = MekariSso::Sender.new
      sender >> MekariSso::MfaDisable.new(@options, user)
    end
  end
end
