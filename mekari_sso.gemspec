Gem::Specification.new do |s|
  s.name = %q{mekari_sso}
  s.version = "2.0.7"
  s.date = %q{2024-09-24}
  s.summary = %q{SDK Ruby for supporting the implementation of Mekari SSO}
  s.files = Dir['lib/**/*.rb']
  s.require_paths = ['lib']
  s.authors = ['WSPDV']
  s.email = 'dwi.heru@mekari.com'
  s.homepage = 'https://gitlab.com/dwiheruwaspodo/mekari_sso'
  s.license = 'MIT'
  s.add_dependency 'rest-client', '~> 2.1'
  s.add_dependency 'json', '~> 2.6', '>= 2.6.1'
  s.add_dependency 'uri', '~> 0.12.0'
  s.add_development_dependency 'rspec', '~> 3.0'
end