# README

Hae, gaes ~ ini buat support SSO di mekari

Berikut yang harus diperhatikan yaaa ~

- Ruby version
  - 3.0.3
- System dependencies
  - rest-client
  - json
  - rspec

## 1. Installation

### Using Gemfile

Add gem mekari_sso to Gemfile

```ruby
gem 'mekari_sso'
```

Run this command in your terminal

```ruby
gem install mekari_sso
```

```ruby
bundle install
```

## 2. Usage

Create instance of Mekari SSO

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://sandbox-sso.mekari.com",
    accept_language: 'en',
    scope: 'sso:profile'
  )
```

### 2.1 Client Credentials Grant Type

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://sandbox-sso.mekari.com",
    accept_language: 'en',
    scope: 'sso:profile'
  )

sso.generate_client
```

### 2.2 Authorization Code Grant Type

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://sandbox-sso.mekari.com",
    accept_language: 'en',
    scope: 'sso:profile'
  )

sso.generate_code(code)
```

### 2.3 Refresh Token Grant Type

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://sandbox-sso.mekari.com",
    accept_language: 'en',
    scope: 'sso:profile'
  )

sso.refresh_token(refresh_token_from_response)
```

### 2.4 Revoke

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://sandbox-sso.mekari.com",
    accept_language: 'en',
    scope: 'sso:profile'
  )

sso.revoke_token(bearer_token_used)
```

### 2.5 Register User

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.register({
  email: 'your email',
  first_name: 'your first_name',
  last_name: 'your last_name',
  password: 'your password',
  phone: 'your phone',
  access_token: bearer_client_credential
})
```

### 2.6 Current User

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.current_user(bearer_token)
```

### 2.7 Industries

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.industries({
  access_token: bearer_client_credential,
  page: 1, # optional -> default is 1
  limit: 10, # optional -> default is 10
  version: 'v1' }) # optional -> default is v1
```

### 2.8 Register Company

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.register_company({
  name: 'company name',
  industry_id: 'industry id',
  user_id: 'user_id',
  email: 'company email',
  access_token: bearer_client_credential

  # optional params
  industry_name: '',
  brand_name: '',
  main_address: '',
  secondary_address: '',
  phone: '',
  tax_name: '',
  tax_number: '',
  billing_name: '',
  billing_email: '',
  billing_phone: '',
}))
```

### 2.9 List of Companies in Application

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.companies({
  access_token: bearer_client_credential,
  page: 1 # optional -> default is 1
})
```

### 2.10 List of Provincies

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.provincies({
  access_token: bearer_client_credential,
  page: 1 # optional -> default is 1
})
```

### 2.11 List of City by Province

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.cities({
  access_token: bearer_client_credential,
  province_id: province_id_selected_from_list
  page: 1 # optional -> default is 1
})
```

### 2.11 List of Postal Code

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.postal_codes({
  access_token: bearer_client_credential,
  city_id: city_id_selected_from_list
})
```

### 2.12 Add user as member of Company

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.add_user_to_company({
  access_token: bearer_client_credential,
  user_id: user_id,
  company_id: company_id
})
```

### 2.13 Remove user from Company

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.remove_user_from_company({
  access_token: bearer_client_credential,
  user_id: user_id,
  company_id: company_id
})
```

### 2.14 Member list of Company

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.company_member({
  access_token: bearer_client_credential,
  company_id: company_id
})
```

### 2.15 Update Data User

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.update_user({
  user_id: user_id,
  email: 'your email',
  first_name: 'your first_name',
  last_name: 'your last_name',
  password: 'your password',
  phone: 'your phone',
  access_token: bearer_token
})
```

### 2.16 Update Avatar User

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.update_avatar({
  user_id: user_id,
  avatar: Tempfile,
  access_token: bearer_token
})
```

### 2.17 Verification Email

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.verify_user_email({
  user_id: user_id,
  redirect_path: 'some redirect_path',
  access_token: bearer_token
})
```

### 2.18 Get Current Company

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.current_company(bearer_token)
```

### 2.19 User Existence

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.existence_user({
  key: "email",
  value: "email_checked",
  access_token: bearer_token
})
```

### 2.20 Validate Password

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.validate_password({
  user_id: "user_id",
  password: "password",
  access_token: bearer_token
})
```

### 2.21 Get Companies of Users

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.companies_of_user({
  user_id: "user_id",
  access_token: bearer_token
})
```

### 2.22 Get Current Company of Users

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.current_company_of_user({
  user_id: "user_id",
  access_token: bearer_token
})
```

### 2.23 Update Company

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.update_company({
  company_id: 'company_id',
  name: 'company name',
  industry_id: 'industry id',
  user_id: 'user_id',
  email: 'company email',
  access_token: bearer_client_credential

  # optional params
  industry_name: '',
  brand_name: '',
  main_address: '',
  secondary_address: '',
  phone: '',
  tax_name: '',
  tax_number: '',
  billing_name: '',
  billing_email: '',
  billing_phone: '',
})
```

### 2.24 MFA Enable

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.mfa_enable({
  access_token: bearer_client_credential,
  user_id: user_id,
  company_id: company_id,
  channel: 'email'
})
```

### 2.25 MFA Disable

```ruby
require 'mekari_sso'
sso = MekariSso::Client.new(
    client_id: "your client_id",
    client_secret: "your client_secret",
    base_url: "https://api-sandbox-sso.mekari.com",
    accept_language: 'en'
  )

sso.mfa_disable({
  access_token: bearer_client_credential,
  user_id: user_id,
  company_id: company_id,
  channel: 'email'
})
```